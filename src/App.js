import React, {useRef} from 'react';
import logo from './logo.svg';
import './App.css';
import QRCode from 'qrcode.react';
import uuid from 'react-uuid'
import { usePeerData } from 'react-peer-data';

function App() {
    const room = useRef();
    const peerData = usePeerData();

    const [session, setSession] =  React.useState(uuid())
    const [user,setUser] = React.useState("user1")
    const [msg,setMsg] = React.useState("")
    const [rev,setRev] = React.useState("")
    const [state,setState] = React.useState("offline")

    React.useEffect(() => {
            if (room.current) return;
        room.current = peerData.connect(session);
        room.current.on("CONNECT", () => { console.log('connected2'); })
        console.log("Room",room)

        room.current
            .on("participant", participant => {
                participant
                    .on("disconnected", () => { console.log('disconnected', participant.id);setState("offline"); })
                    .on("connected", () => { console.log('connected1', participant);})
                    .on("track", event => { console.log('stream', participant.id, event.streams[0]); })
                    .on("message", payload => {
                        console.log(participant.id, payload);
                        var data = rev + "\n" + payload
                        setRev(data)
                    })
                    .on("error", event => {
                        setState("offline");
                        console.error('peer', participant.id, event);
                        participant.renegotiate();
                    });
            })
            .on("error", event => { console.error('room', "name", event); });

        return () => room.current.disconnect()}
    , [peerData]);

    return (
    <div className="App">
      <header className="App-header">
        <div style={{borderColor: "white",border: "100px"}}>
          <QRCode includeMargin={true} size="300"  value={window.location.href.split("#")[0]  + "#/connect/" + session} />
          <p>{window.location.href.split("#")[0]  + "#/connect/" + session}</p>
            <p>{rev}</p>
            <p>{state}</p>
        </div>
          <input type="text" onChange={e => setMsg(e.target.value)}/>
          <button onClick={() => {
              if (room.current) {
                  room.current.send(msg);
          }
             }
          }>send</button>
      </header>
    </div>
    );
}

export default App;
