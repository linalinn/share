import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { HashRouter, Route} from "react-router-dom";
import {Switch} from "react-bootstrap";
import { PeerDataProvider } from 'react-peer-data';
import Slave from "./slave";

ReactDOM.render(
    <HashRouter>
    <PeerDataProvider
        servers={{ iceServers: [{ url: "stun:stun.1.google.com:19302" }] }}
        constraints={{ ordered: true }}
        signaling={{ url: "https://share.u2s.cloud/"}}
    >
        <Switch>
            <Route exact path="/" component={App}/>
            <Route path="/connect/:id" component={Slave}/>
        </Switch>
    </PeerDataProvider>
    </HashRouter>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
/*<BrowserRouter>
          <Switch>
              <Route exact path="/" component={App}/>
              <Route exact path="/nosession" component={App}/>
          </Switch>
      </BrowserRouter>*/
