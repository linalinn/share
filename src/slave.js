import React, {useRef} from 'react';
import './App.css';
import { usePeerData } from 'react-peer-data';
import { useParams } from 'react-router-dom'

function Slave() {
  const room = useRef();
  const peerData = usePeerData();
    const [msg,setMsg] = React.useState("")
    const [rev,setRev] = React.useState("")
    const id = useParams()
    const [state,setState] = React.useState("offline")


    React.useEffect(() => {
            if (room.current) return;
        room.current = peerData.connect(id.id);
        console.log(id.id);
        room.current.on("CONNECT", () => { console.log('connected2'); })

        room.current.on("connected", e => {console.log("LOL",e)});

        room.current
            .on("participant", participant => {
                setState("online")
                participant
                    .on("disconnected", () => { console.log('disconnected', participant.id);setState("offline"); })
                    .on("connected", () => { console.log('connected1', participant); })
                    .on("track", event => {console.log('stream', participant.id, event.streams[0]); })
                    .on("message", payload => {
                        console.log(participant.id, payload);
                        var data = rev + "\n" + payload
                        setRev(data)
                    })
                    .on("error", event => {
                        console.error('peer', participant.id, event);
                        setState("error")
                        participant.renegotiate();
                    });
            })
            .on("error", event => { console.error('room', "name", event); });
        room.current.send("Hi");

        return () => room.current.disconnect()}
    , [peerData]);

  return (
    <div className="App">
      <header className="App-header">
          <p>{rev}</p>
          <p>{state}</p>
          <input type="text" onChange={e => setMsg(e.target.value)}/>
          <button onClick={() => {
              if (room.current) {
                  room.current.send(msg);
          }
             }
          }>send</button>
      </header>
    </div>
  );
}

export default Slave;
